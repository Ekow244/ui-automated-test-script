package test.basicWeb;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LaravelUpgradeTestCases {
    WebDriver driver;
    String baseUrl;
    String baseUrl1;
    String baseUrl2;
    String baseUrl3;


    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "/home/dev2/eclipse-workspace/SeleniumProject/libs/geckodriver");
        driver=new FirefoxDriver();
        JavascriptExecutor js;
        baseUrl="https://192.168.3.5/home";
        baseUrl1="https://192.168.3.5";
        baseUrl2="https://darkmatter.votomobile.org";
        baseUrl3="https://darkmatter3.votomobile.org";
        js=(JavascriptExecutor)driver;



    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void createSubscriber(){
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();
        driver.findElement(By.cssSelector(".pull-right .btn-primary:nth-child(1)")).click();

        driver.findElement(By.cssSelector("[name='phone']")).sendKeys("0554586956");
        driver.findElement(By.cssSelector("[class='col-sm-6'] .form-group:nth-of-type(1) .form-control")).sendKeys("Newly Created Subscriber Now");
        //Scroll down
        js.executeScript("window.scrollBy(0, 1900);");

        driver.findElement(By.cssSelector(".btn.btn-primary")).click();


    }

    @Test
    public void addingSubscriberToGroup(){
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();
        driver.findElement(By.cssSelector(".pull-right .btn-primary:nth-child(1)")).click();

        driver.findElement(By.cssSelector("[name='phone']")).sendKeys("0554586956");

        driver.findElement(By.cssSelector("[type='search']")).click();
        driver.findElement(By.cssSelector("[role='treeitem']")).click();


        driver.findElement(By.cssSelector("[class='col-sm-6'] .form-group:nth-of-type(1) .form-control")).sendKeys("Newly Created Subscriber Now");
        //Scroll down
        js.executeScript("window.scrollBy(0, 1900);");

        driver.findElement(By.cssSelector(".btn.btn-primary")).click();


    }

    @Test
    public void createSubscriberWithSms(){
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();
        driver.findElement(By.cssSelector(".pull-right .btn-primary:nth-child(1)")).click();

        driver.findElement(By.cssSelector("[name='phone']")).sendKeys("0554586956");
        driver.findElement(By.cssSelector("[class='col-sm-6'] .form-group:nth-of-type(1) .form-control")).sendKeys("Newly Created Subscriber Now");
        //Scroll down
        js.executeScript("window.scrollBy(0, 1900);");

        driver.findElement(By.cssSelector("[name='receive_sms']")).click();
        driver.findElement(By.cssSelector(".btn.btn-primary")).click();


    }

    @Test
    public void addSubscriberToGroup(){
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();


        driver.findElement(By.cssSelector("[data-key='campaigns'] [effect]")).click();

        driver.findElement(By.cssSelector(".slider-menu-item:nth-of-type(4) .font-weight-medium")).click();


        driver.findElement(By.cssSelector(".outgoing-calls-page > .row:nth-child(2) [class='col-xs-12 col-sm-5'] .input-block-level.form-control")).click();
        driver.findElement(By.cssSelector("[value='all']")).click();

        driver.findElement(By.cssSelector(".outgoing-calls-page .inline:nth-child(9) [type]")).click();

        js.executeScript("window.scrollBy(0, 3000);");
        driver.findElement(By.cssSelector(".button-container .btn-primary")).click();


        driver.findElement(By.cssSelector(".btn-loading.btn")).click();




    }


    @Test
    public void removeSubscriberFromGroup(){
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();


        driver.findElement(By.cssSelector("[data-key='campaigns'] [effect]")).click();

        driver.findElement(By.cssSelector(".slider-menu-item:nth-of-type(4) .font-weight-medium")).click();


        driver.findElement(By.cssSelector(".outgoing-calls-page > .row:nth-child(2) [class='col-xs-12 col-sm-5'] .input-block-level.form-control")).click();
        driver.findElement(By.cssSelector("[value='all']")).click();

        driver.findElement(By.cssSelector(".outgoing-calls-page .inline:nth-child(9) [type]")).click();

        //js.executeScript("window.scrollBy(0, 3000);");
        driver.findElement(By.cssSelector(".button-container .btn-primary")).click();

        js.executeScript("window.scrollBy(30, 5000);");


        driver.findElement(By.cssSelector(".btn-loading.btn")).click();




    }


    @Test
    public void deleteSubscriber(){
        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();

        driver.findElement(By.cssSelector("tr:nth-of-type(1) .js-delete")).click();
        driver.findElement(By.cssSelector("#deleteSubmit")).click();
    }

    @Test
    public void editSubscriber(){
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;
        driver.get(baseUrl2);

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();

        driver.findElement(By.cssSelector("tr:nth-of-type(1) td:nth-of-type(9) .btn-default:nth-of-type(1)")).click();

        driver.findElement(By.cssSelector(".row [class='col-sm-6']:nth-of-type(2) .form-group:nth-of-type(1) .form-control")).sendKeys("r/subscriber edited");
        js.executeScript("window.scrollBy(0, 1900);");

        driver.findElement(By.cssSelector(".btn-primary")).click();
    }



    @Test
    public void callToRecord(){
        driver.get(baseUrl2);


        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-content'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='messages']")).click();
        driver.findElement(By.cssSelector(".ga-messages-index-new")).click();
        driver.findElement(By.cssSelector(".form-control")).sendKeys("New Message Created");
        driver.findElement(By.cssSelector("#show_voice_box .audio-choice:nth-of-type(1) .btn-group:nth-of-type(1) .dropdown-toggle")).click();
        driver.findElement(By.cssSelector("#show_voice_box .audio-choice:nth-of-type(1) .js-audio-choice-calltorecord-btn")).click();
        driver.findElement(By.cssSelector("#calltorecord_caller_select_0")).click();
        driver.findElement(By.cssSelector("#calltorecord-modal-btn-call")).click();
    }

    @Test
    public void exportSubscribers(){
        driver.get(baseUrl2);
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;



        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();

        driver.findElement(By.cssSelector(".dropdown-toggle")).click();

        driver.findElement(By.xpath("/html//div[@id='viamo-app']//ul[@class='dropdown-menu pull-right']//a[@href='https://192.168.3.5/subscribers/export?group=0']")).click();

        driver.findElement(By.cssSelector(".btn-primary")).click();




    }


    @Test
    public void exportSubscribersToExcelSupportedCsv(){
        driver.get(baseUrl2);
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;


        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
        driver.findElement(By.cssSelector("[rel='subscribers']")).click();

        driver.findElement(By.cssSelector(".dropdown-toggle")).click();

        driver.findElement(By.xpath("/html//div[@id='viamo-app']//ul[@class='dropdown-menu pull-right']//a[@href='https://darkmatter.votomobile.org/subscribers/export?group=0']")).click();

        driver.findElement(By.cssSelector("[method='POST'] > .row:nth-child(3) > [class='col-sm-3 form-group']:nth-child(3) .input-medium.form-control")).click();

        driver.findElement(By.cssSelector(".btn-primary")).click();



    }

    @Test
    public void sendOutboundCampaign(){
        driver.get(baseUrl2);
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();

        driver.findElement(By.cssSelector("[data-key='campaigns'] [effect]")).click();
        driver.findElement(By.cssSelector(".slider-menu-item:nth-of-type(4) .font-weight-medium")).click();
        driver.findElement(By.cssSelector(".outgoing-calls-page > .row:nth-child(2) [class='col-xs-12 col-sm-5'] .input-block-level.form-control")).click();

        driver.findElement(By.cssSelector("[value='all']")).click();

        js.executeScript("window.scrollBy(0, 3000);");
        driver.findElement(By.cssSelector(".button-container .btn-primary")).click();


        driver.findElement(By.cssSelector(".btn-loading.btn")).click();
    }

    @Test
    public void groupExpiry(){
        driver.get(baseUrl2);
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();


        driver.findElement(By.cssSelector("[data-key='campaigns'] [effect]")).click();

        driver.findElement(By.cssSelector(".slider-menu-item:nth-of-type(4) .font-weight-medium")).click();


        driver.findElement(By.cssSelector(".outgoing-calls-page > .row:nth-child(2) [class='col-xs-12 col-sm-5'] .input-block-level.form-control")).click();
        driver.findElement(By.cssSelector("[value='all']")).click();

        driver.findElement(By.cssSelector(".outgoing-calls-page .inline:nth-child(9) [type]")).click();

        //js.executeScript("window.scrollBy(0, 3000);");
        driver.findElement(By.cssSelector(".button-container .btn-primary")).click();

        js.executeScript("window.scrollBy(30, 5000);");


        driver.findElement(By.cssSelector(".btn-loading.btn")).click();
    }

    @Test
    public void editSubscriberViaTree(){
        driver.get(baseUrl2);
        JavascriptExecutor js;
        js=(JavascriptExecutor)driver;

        driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
        driver.findElement(By.id("password")).sendKeys("0v3r10rD");
        driver.findElement(By.cssSelector("[method] [type='submit']")).click();


        driver.findElement(By.cssSelector("[data-key='campaigns'] [effect]")).click();

        driver.findElement(By.cssSelector(".slider-menu-item:nth-of-type(4) .font-weight-medium")).click();


        driver.findElement(By.cssSelector(".outgoing-calls-page > .row:nth-child(2) [class='col-xs-12 col-sm-5'] .input-block-level.form-control")).click();
        driver.findElement(By.cssSelector("[value='all']")).click();

        driver.findElement(By.cssSelector(".outgoing-calls-page .inline:nth-child(9) [type]")).click();

        //js.executeScript("window.scrollBy(0, 3000);");
        driver.findElement(By.cssSelector(".button-container .btn-primary")).click();

        js.executeScript("window.scrollBy(30, 5000);");


        driver.findElement(By.cssSelector(".btn-loading.btn")).click();
    }


}