package basicWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.JavascriptExecutor;




import java.util.Scanner;


public class GenericTestFile {

	


    public static void main(String[] args) {
		// TODO Auto-generated method stub
		//http://chromedriver.storage.googleapis.com/index.html


		Scanner user_input= new Scanner(System.in);
		System.out.println("Please enter the url of the server you want to test:");


		String baseUrl=user_input.next();
		System.out.println("Please enter the email of your account: ");
		String user_email=user_input.next();
		System.out.println("Please enter the password of your account: ");
		String user_password=user_input.next();
		

		
		JavascriptExecutor js;
	
		WebDriver driver;
		
		System.setProperty("webdriver.gecko.driver", "/home/dev2/eclipse-workspace/SeleniumProject/libs/geckodriver");
		
		driver=new FirefoxDriver();
		js=(JavascriptExecutor)driver;
		


		//driver.findElement(By.xpath("")).click();
		//driver.findElement(By.cssSelector("cssSelector")).click();
		

		driver.get(baseUrl);
      
		driver.findElement(By.id("email")).sendKeys(user_email);
		driver.findElement(By.id("password")).sendKeys(user_password);
		driver.findElement(By.cssSelector("[method] [type='submit']")).click();



		
	   
		
		
		//Create Subscriber
		driver.findElement(By.cssSelector("[data-key='campaign-subscribers'] [effect]")).click();
		driver.findElement(By.cssSelector("[rel='subscribers']")).click();
		driver.findElement(By.cssSelector(".pull-right .btn-primary:nth-child(1)")).click();

		driver.findElement(By.cssSelector("[name='phone']")).sendKeys("0554586956");
		driver.findElement(By.cssSelector("[class='col-sm-6'] .form-group:nth-of-type(1) .form-control")).sendKeys("Newly Created Subscriber Now");
		//Scroll down
		js.executeScript("window.scrollBy(0, 1900);");

		driver.findElement(By.cssSelector(".btn.btn-primary")).click();
		
		//Delete subscriber
		driver.findElement(By.cssSelector("tr:nth-of-type(1) .js-delete")).click();
		driver.findElement(By.cssSelector("#deleteSubmit")).click();

		
		//Edit subscriber
		driver.findElement(By.cssSelector("tr:nth-of-type(1) td:nth-of-type(9) .btn-default:nth-of-type(1)")).click();
	
		driver.findElement(By.cssSelector(".row [class='col-sm-6']:nth-of-type(2) .form-group:nth-of-type(1) .form-control")).sendKeys("r/subscriber edited");
		js.executeScript("window.scrollBy(0, 1900);");
		
		driver.findElement(By.cssSelector(".btn-primary")).click();


		//CTR
		driver.findElement(By.cssSelector("[data-key='campaign-content'] [effect]")).click();
		driver.findElement(By.cssSelector("[rel='messages']")).click();
		driver.findElement(By.cssSelector(".ga-messages-index-new")).click();
		driver.findElement(By.cssSelector(".form-control")).sendKeys("New Message Created");
		driver.findElement(By.cssSelector("#show_voice_box .audio-choice:nth-of-type(1) .btn-group:nth-of-type(1) .dropdown-toggle")).click();
		driver.findElement(By.cssSelector("#show_voice_box .audio-choice:nth-of-type(1) .js-audio-choice-calltorecord-btn")).click();
		driver.findElement(By.cssSelector("#calltorecord_caller_select_0")).click();
		driver.findElement(By.cssSelector("#calltorecord-modal-btn-call")).click();


		//Sending already existing content to subscribers
		driver.findElement(By.cssSelector("[data-key='campaigns'] [effect]")).click();
		driver.findElement(By.cssSelector(".slider-menu-item:nth-of-type(4) .font-weight-medium")).click();
		driver.findElement(By.cssSelector(".outgoing-calls-page > .row:nth-child(2) [class='col-xs-12 col-sm-5'] .input-block-level.form-control")).click();

		driver.findElement(By.cssSelector("[value='all']")).click();
		driver.findElement(By.cssSelector(".button-container .btn-primary")).click();

		driver.findElement(By.cssSelector(".btn-loading.btn")).click();




		
		
		
	}

}
