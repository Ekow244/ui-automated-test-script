package basicWeb;

import org.openqa.selenium.By;
import java.util.Scanner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SmokeTests {
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//http://chromedriver.storage.googleapis.com/index.html
		Scanner user_input= new Scanner(System.in);
		System.out.println("Please enter the url of the server you want to test: ");
		String base_url=user_input.next();
		
		
		WebDriver driver;
		
		System.setProperty("webdriver.gecko.driver", "/home/dev2/eclipse-workspace/SeleniumProject/libs/geckodriver");
		
		driver=new FirefoxDriver();

		
		//String baseUrl ="https://darkmatter.votomobile.org/";
        String baseUrl =base_url;
		
		//driver.findElement(By.xpath("")).click();
		
		driver.get(baseUrl);
		driver.findElement(By.id("email")).sendKeys("daniel.melfah@viamo.io");
		driver.findElement(By.id("password")).sendKeys("0v3r10rD");
		driver.findElement(By.xpath("/html/body//form[@action='https://darkmatter.votomobile.org']/input[@value='Submit']")).click();
		
		

	   
		
		
		//Create Subscriber
		driver.findElement(By.xpath("//div[@id='viamo-app']//div[@class='bs-docs-sidebar']/ul/li[2]//span[1]")).click();
		driver.findElement(By.xpath("//div[@id='viamo-app']/div[@class='sidebar-container']/div[1]//div[@class='aside-body']/div[2]/ul/li[@class='slider-menu-item']/div/div/ul/li[1]//ul/li[1]//a[@href='https://darkmatter.votomobile.org/subscribers']")).click();
		driver.findElement(By.xpath("/html//div[@id='viamo-app']//a[@href='https://darkmatter.votomobile.org/subscriber/new']")).click();

		driver.findElement(By.id("phone")).sendKeys("0554586956");
		driver.findElement(By.xpath("/html//div[@id='viamo-app']/div[@class='container-full']//form[@action='https://darkmatter.votomobile.org/subscriber/new']//input[@name='fields[18714]']")).sendKeys("Newly Created Subscriber Now");
		driver.findElement(By.xpath("/html//div[@id='viamo-app']//form[@action='https://darkmatter.votomobile.org/subscriber/new']//button[@type='submit']")).click();
		
		//Delete subscriber
		driver.findElement(By.xpath("/html//form[@id='frmCheck']/table[@class='table table-striped']/tbody/tr[1]/td[9]/a[@title='Delete Subscriber']")).click();
		driver.findElement(By.xpath("/html//button[@id='deleteSubmit']")).click();
		
		
		//Edit subscriber
		driver.findElement(By.xpath("/html//form[@id='frmCheck']/table[@class='table table-striped']/tbody/tr[1]/td[9]/a[@title='Edit Details']")).click();
	
		driver.findElement(By.name("fields[18714]")).sendKeys("r/subscriber edited");
		
		driver.findElement(By.cssSelector(".btn-primary")).click();
		
		


		
		
		
	}

}
